import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    mode: "history",
    routes: [
        {
            path: "/",
            alias: "/tutorials",
            name: "tutorials",
            component: () => import("./components/StudentList")
        },
        {
            path: "/tutorials#tags",
            name: "tutorial-details",
            component: () => import("./components/Filters")
        },
        {
            path: "/add",
            name: "add",
            component: () => import("./components/Tags")
        }
    ]
});