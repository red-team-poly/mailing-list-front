window.onresize = function() {
    // window.onresize = function () {
    //     let tableUserHeader = document.querySelector('.block-users .tbl-header').clientHeight;
    //     let tableUserContent = document.querySelector('.block-users .tbl-content');
    //     let newHieghtTableUser = tableUserHeader + 10;
    //     tableUserContent.style.top = newHieghtTableUser.toString() + 'px';
    // };

    if (document.documentElement.clientWidth >= 961) {
        document.body.style.overflow = 'hidden';
    } else {
        document.body.style.overflow = 'visible';
    }
};

document.addEventListener('DOMContentLoaded', function() {
    let table = document.querySelector('.tbl-content table');
    let headersStudent = document.querySelectorAll('.block-students .tbl-header th:not(:nth-child(1))');
    const tableBody = table.querySelector('tbody');
    const rows = tableBody.querySelectorAll('tr');

    // Направление сортировки
    const directions = Array.from(headersStudent).map(function() {
        return '';
    });

    const sortColumn = function(index) {
        const direction = directions[index] || 'asc';

        // Фактор по направлению
        const multiplier = (direction === 'asc') ? 1 : -1;

        const newRows = Array.from(rows);

        newRows.sort(function (rowA, rowB) {
            const cellA = rowA.querySelectorAll('td')[index + 1].innerHTML;
            const cellB = rowB.querySelectorAll('td')[index + 1].innerHTML;

            switch (true) {
                case cellA > cellB:
                    return 1 * multiplier;
                case cellA < cellB:
                    return -1 * multiplier;
                case cellA === cellB:
                    return 0;
            }
        });

        // Удалить старые строки
        [].forEach.call(rows, function (row) {
            tableBody.removeChild(row);
        });

        // Поменять направление
        directions[index] = direction === 'asc' ? 'desc' : 'asc';

        // Добавить новую строку
        newRows.forEach(function (newRow) {
            tableBody.appendChild(newRow);
        });

        for (let i = 0; i < headersStudent.length; i++) {
            headersStudent[i].className = "";
            if (direction === 'asc')
                headersStudent[index].className = "sort-down";
            else
                headersStudent[index].className = "sort-up";
        }
    };

    [].forEach.call(headersStudent, function(e, index) {
        e.addEventListener('click', function() {
            sortColumn(index);
        });
    });
});
