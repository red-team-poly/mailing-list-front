import http from "../http-common";

class StudentService {
    getAll() {
        return http.get("/students");
    }
}

export default new StudentService();