window.onresize = function() {
    let block = document.querySelector('.left-column-25').clientHeight;

    let inblocktagup = document.querySelector('.tag-categories').clientHeight;
    let inblocktagdown = document.querySelector('.tag-list');
    let newHieghtTag = ((100 * inblocktagup) / block) + 5;
    inblocktagdown.style.top = newHieghtTag.toString() + '%';

    let inblockfilterup = document.querySelector('.filter-categories').clientHeight;
    let inblockfilterdown = document.querySelector('.filter-list');
    let newHieghtFilter = ((100 * inblockfilterup) / block) + 5;
    inblockfilterdown.style.top = newHieghtFilter.toString() + '%';

    if (document.documentElement.clientWidth >= 961) {
        document.body.style.overflow = 'hidden';
    } else {
        document.body.style.overflow = 'visible';
    }
};

setInterval(function () {
    if (document.querySelector('.block-users')) {
        window.onresize = function () {
            let tableUserHeader = document.querySelector('.block-users .tbl-header').clientHeight;
            let tableUserContent = document.querySelector('.block-users .tbl-content');
            let newHieghtTableUser = tableUserHeader + 10;
            tableUserContent.style.top = newHieghtTableUser.toString() + 'px';
        };
    }
}, 500);

/* ================================================= РАБОТА С ПОИСКОМ =============================================== */

let listTag = document.querySelectorAll('.list-tag li');
let listFilter = document.querySelectorAll('.filters button');

document.getElementById('search-tag').onkeyup = function () {
    this.value = this.value.toLowerCase();
    let check = 0;

    if (radiosTag[1].checked) {
        document.querySelectorAll('.list-tag li[data-type="public"]').forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })

        for (let i = 0; i < listTag.length; i++)
            if (listTag[i].getAttribute('style') === "display: none;")
                check++;
    } else if (radiosTag[2].checked) {
        document.querySelectorAll('.list-tag li[data-type="local"], .list-tag li[data-type="publiclocal"]').forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })

        for (let i = 0; i < listTag.length; i++)
            if (listTag[i].getAttribute('style') === "display: none;")
                check++;
    } else if (radiosTag[3].checked) {
        document.querySelectorAll('.list-tag li[data-type="publiclocal"]').forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })

        for (let i = 0; i < listTag.length; i++)
            if (listTag[i].getAttribute('style') === "display: none;")
                check++;
    }
    else {
        listTag.forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })

        for (let i = 0; i < listTag.length; i++)
            if (listTag[i].getAttribute('style') === "display: none;")
                check++;
    }

    if (check === listTag.length)
        document.getElementById('nonresulttag').style.display = 'block';
    else
        document.getElementById('nonresulttag').style.display = 'none';
};
document.getElementById('search-tag').addEventListener('search', function () {
    document.getElementById('nonresulttag').style.display = 'none';
    for (let i = 0; i < listTag.length; i++)
        listTag[i].style.display = 'inline';

    if (radiosTag[1].checked) {
        for (let i = 0; i < listTag1.length; i++) {
            listTag1[i].style.display = 'inline';
            if (listTag1[i].getAttribute('data-type') !== "public")
                listTag1[i].style.display = 'none';
        }
    }

    if (radiosTag[2].checked) {
        for (let i = 0; i < listTag1.length; i++) {
            listTag1[i].style.display = 'inline';
            if (listTag1[i].getAttribute('data-type') !== "local" &&
                listTag1[i].getAttribute('data-type') !== "publiclocal")
                listTag1[i].style.display = 'none';
        }
    }

    if (radiosTag[3].checked) {
        for (let i = 0; i < listTag1.length; i++) {
            listTag1[i].style.display = 'inline';
            if (listTag1[i].getAttribute('data-type') !== "publiclocal")
                listTag1[i].style.display = 'none';
        }
    }
});

document.getElementById('search-filters').onkeyup = function() {
    this.value = this.value.toLowerCase();
    let check = 0;

    if (radiosFilter[0].checked) {
        document.querySelectorAll('.filters button[data-type="public"]').forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })

        for (let i = 0; i < listFilter.length; i++)
            if (listFilter[i].getAttribute('style') === "display: none;")
                check++;
    } else if (radiosFilter[1].checked) {
        document.querySelectorAll('.filters button[data-type="local"], .filters button[data-type="publiclocal"]').forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })

        for (let i = 0; i < listFilter.length; i++)
            if (listFilter[i].getAttribute('style') === "display: none;")
                check++;
    } else if (radiosFilter[2].checked) {
        document.querySelectorAll('.filters button[data-type="publiclocal"]').forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })

        for (let i = 0; i < listFilter.length; i++)
            if (listFilter[i].getAttribute('style') === "display: none;")
                check++;
    } else {
        listFilter.forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })

        for (let i = 0; i < listFilter.length; i++)
            if (listFilter[i].getAttribute('style') === "display: none;")
                check++;
    }

    if (check === listFilter.length)
        document.getElementById('nonresultfilter').style.display = 'block';
    else
        document.getElementById('nonresultfilter').style.display = 'none';
};
document.getElementById('search-filters').addEventListener('search', function () {
    document.getElementById('nonresultfilter').style.display = 'none';
    for (let i = 0; i < listFilter.length; i++)
        listFilter[i].style.display = '';

    if (radiosFilter[0].checked) {
        for (let i = 0; i < listFilter1.length; i++) {
            listFilter1[i].style.display = 'inline';
            if (listFilter1[i].getAttribute('data-type') !== "public")
                listFilter1[i].style.display = 'none';
        }
    }

    if (radiosFilter[1].checked) {
        for (let i = 0; i < listFilter1.length; i++) {
            listFilter1[i].style.display = 'inline';
            if (listFilter1[i].getAttribute('data-type') !== "local" &&
                listFilter1[i].getAttribute('data-type') !== "publiclocal")
                listFilter1[i].style.display = 'none';
        }
    }

    if (radiosFilter[2].checked) {
        for (let i = 0; i < listFilter1.length; i++) {
            listFilter1[i].style.display = 'inline';
            if (listFilter1[i].getAttribute('data-type') !== "publiclocal")
                listFilter1[i].style.display = 'none';
        }
    }
});

document.getElementById('search-student').onkeyup = function() {
    this.value = this.value.toLowerCase();
    let check = 0;

    if (/^(1|[1-5]\d*)$/.test(this.value) && this.value.length === 1) {
        document.querySelectorAll('#table-student tr td:nth-child(4)').forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.parentElement.style.display = '';
            else
                e.parentElement.style.display = 'none';
        })
    } else {
        document.querySelectorAll('#table-student tr').forEach(e => {
            let s = e.textContent.toLowerCase().includes(this.value);
            if (s)
                e.style.display = '';
            else
                e.style.display = 'none';
        })
    }

    for (let i = 0; i < document.querySelectorAll('#table-student tr').length; i++)
        if (document.querySelectorAll('#table-student tr')[i].getAttribute('style') === "display: none;")
            check++;

    if (check === document.querySelectorAll('#table-student tr').length)
        document.getElementById('nonresultstudent').style.display = 'block';
    else
        document.getElementById('nonresultstudent').style.display = 'none';
};
document.getElementById('search-student').addEventListener('search', function () {
    document.getElementById('nonresultstudent').style.display = 'none';
    for (let i = 0; i < document.querySelectorAll('#table-student tr').length; i++)
        document.querySelectorAll('#table-student tr')[i].style.display = '';
});

setInterval(function () {
    if (document.querySelector("#search-user")) {
        document.getElementById('search-user').onkeyup = function() {
            this.value = this.value.toLowerCase();
            let check = 0;

            document.querySelectorAll('#table-user tr').forEach(e => {
                let s = e.textContent.toLowerCase().includes(this.value);
                if (s)
                    e.style.display = '';
                else
                    e.style.display = 'none';
            })

            for (let i = 0; i < document.querySelectorAll('#table-user tr').length; i++)
                if (document.querySelectorAll('#table-user tr')[i].getAttribute('style') === "display: none;")
                    check++;

            if (check === document.querySelectorAll('#table-user tr').length)
                document.getElementById('nonresultstuser').style.display = 'block';
            else
                document.getElementById('nonresultstuser').style.display = 'none';
        };
        document.getElementById('search-user').addEventListener('search', function () {
            document.getElementById('nonresultstuser').style.display = 'none';
            for (let i = 0; i < document.querySelectorAll('#table-user tr').length; i++)
                document.querySelectorAll('#table-user tr')[i].style.display = '';
        });
    }
}, 5000);

let selectTagFilter = document.getElementById('select-dropdown');
let listTagFilter = document.querySelectorAll('.list-tags-for-filter button');
let tagsArrayAll = [];
let tagsArray = [];

for (let i = 0; i < listTagFilter.length; i++) {
    let tag = listTagFilter[i].innerHTML.split("<");
    tagsArrayAll.push(tag[0].toLowerCase());
}

document.getElementById('search-tag-for-filter').onkeyup = function () {
    this.value = this.value.toLowerCase();

    let l = this.value.length;
    let check = 0;

    if (l === 0) {
        for (let i = 0; i < listTagFilter.length; i++) {
            listTagFilter[i].style.display = 'inline';
        }
        check = 0;
        document.getElementById('nonresulttagforfilter').style.display = 'none';
    }

    if (l > 0) {
        for (let i = 0; i < tagsArrayAll.length; i++) {
            listTagFilter[i].style.display = 'inline';
            let s = tagsArrayAll[i].split('').slice(0, l).join('');
            if (s !== this.value) {
                listTagFilter[i].style.display = 'none';
                check++;
            }

            if (check === tagsArrayAll.length)
                document.getElementById('nonresulttagforfilter').style.display = 'block';
            else
                document.getElementById('nonresulttagforfilter').style.display = 'none';
        }
    }
};
selectTagFilter.addEventListener('change', function() {
    document.getElementById('nonresulttagforfilter').style.display = 'none';
    document.getElementById('search-tag-for-filter').value = '';
    if (selectTagFilter.selectedIndex === 0) {
        tagsArrayAll = [];
        for (let i = 0; i < listTagFilter.length; i++) {
            listTagFilter[i].style.display = 'inline';
            let tagsAll = listTagFilter[i].innerHTML.split("<");
            tagsArrayAll.push(tagsAll[0].toLowerCase());
        }

        document.getElementById('search-tag-for-filter').onkeyup = function () {
            this.value = this.value.toLowerCase();

            let l = this.value.length;
            let check = 0;

            if (l === 0) {
                for (let i = 0; i < listTagFilter.length; i++) {
                    listTagFilter[i].style.display = 'inline';
                }
                check = 0;
                document.getElementById('nonresulttagforfilter').style.display = 'none';
            }

            if (l > 0) {
                for (let i = 0; i < tagsArrayAll.length; i++) {
                    listTagFilter[i].style.display = 'inline';
                    let s = tagsArrayAll[i].split('').slice(0, l).join('');
                    if (s !== this.value) {
                        listTagFilter[i].style.display = 'none';
                        check++;
                    }

                    if (check === tagsArrayAll.length)
                        document.getElementById('nonresulttagforfilter').style.display = 'block';
                    else
                        document.getElementById('nonresulttagforfilter').style.display = 'none';
                }
            }
        };
    }

    if (selectTagFilter.selectedIndex === 1) {
        tagsArray = [];
        for (let i = 0; i < listTagFilter.length; i++) {
            listTagFilter[i].style.display = 'inline';
            if (listTagFilter[i].getAttribute('data-type') !== "local" &&
                listTagFilter[i].getAttribute('data-type') !== "publiclocal")
                listTagFilter[i].style.display = 'none';
            else {
                let tagsLocal = listTagFilter[i].innerHTML.split("<");
                tagsArray.push(tagsLocal[0].toLowerCase());
            }
        }

        document.getElementById('search-tag-for-filter').onkeyup = function() {
            this.value = this.value.toLowerCase();

            let l = this.value.length;
            let check = 0;

            if (l === 0) {
                for (let i = 0; i < listTagFilter.length; i++) {
                    if (listTagFilter[i].getAttribute('data-type') === "local" ||
                        listTagFilter[i].getAttribute('data-type') === "publiclocal") {
                        listTagFilter[i].style.display = 'inline';
                    }
                }
                check = 0;
                document.getElementById('nonresulttagforfilter').style.display = 'none';
            }

            if (l > 0) {
                for (let i = 0; i < tagsArrayAll.length; i++) {
                    if (listTagFilter[i].getAttribute('data-type') === "local" ||
                        listTagFilter[i].getAttribute('data-type') === "publiclocal") {
                        listTagFilter[i].style.display = 'inline';
                        let s = tagsArrayAll[i].split('').slice(0, l).join('');
                        if (s !== this.value) {
                            listTagFilter[i].style.display = 'none';
                            check++;
                        }

                        if (check === tagsArray.length)
                            document.getElementById('nonresulttagforfilter').style.display = 'block';
                        else
                            document.getElementById('nonresulttagforfilter').style.display = 'none';
                    }
                }
            }
        };
    }

    if (selectTagFilter.selectedIndex === 2) {
        tagsArray = [];
        for (let i = 0; i < listTagFilter.length; i++) {
            listTagFilter[i].style.display = 'inline';
            if (listTagFilter[i].getAttribute('data-type') !== "public" &&
                listTagFilter[i].getAttribute('data-type') !== "publiclocal")
                listTagFilter[i].style.display = 'none';
            else {
                let tagsLocal = listTagFilter[i].innerHTML.split("<");
                tagsArray.push(tagsLocal[0].toLowerCase());
            }
        }

        document.getElementById('search-tag-for-filter').onkeyup = function() {
            this.value = this.value.toLowerCase();

            let l = this.value.length;
            let check = 0;

            if (l === 0) {
                for (let i = 0; i < listTagFilter.length; i++) {
                    if (listTagFilter[i].getAttribute('data-type') === "public" ||
                        listTagFilter[i].getAttribute('data-type') === "publiclocal") {
                        listTagFilter[i].style.display = 'inline';
                    }
                }
                check = 0;
                document.getElementById('nonresulttagforfilter').style.display = 'none';
            }

            if (l > 0) {
                for (let i = 0; i < tagsArrayAll.length; i++) {
                    if (listTagFilter[i].getAttribute('data-type') === "public" ||
                        listTagFilter[i].getAttribute('data-type') === "publiclocal") {
                        listTagFilter[i].style.display = 'inline';
                        let s = tagsArrayAll[i].split('').slice(0, l).join('');
                        if (s !== this.value) {
                            listTagFilter[i].style.display = 'none';
                            check++;
                        }

                        if (check === tagsArray.length)
                            document.getElementById('nonresulttagforfilter').style.display = 'block';
                        else
                            document.getElementById('nonresulttagforfilter').style.display = 'none';
                    }
                }
            }
        };
    }
});
document.getElementById('search-tag-for-filter').addEventListener('search', function () {
    document.getElementById('nonresulttagforfilter').style.display = 'none';
    if (selectTagFilter.selectedIndex === 0) {
        for (let i = 0; i < listTagFilter.length; i++)
            listTagFilter[i].style.display = 'inline';
    }

    if (selectTagFilter.selectedIndex === 1) {
        for (let i = 0; i < listTagFilter.length; i++) {
            listTagFilter[i].style.display = 'inline';
            if (listTagFilter[i].getAttribute('data-type') !== "local" &&
                listTagFilter[i].getAttribute('data-type') !== "publiclocal")
                listTagFilter[i].style.display = 'none';
        }
    }

    if (selectTagFilter.selectedIndex === 2) {
        for (let i = 0; i < listTagFilter.length; i++) {
            listTagFilter[i].style.display = 'inline';
            if (listTagFilter[i].getAttribute('data-type') !== "public" &&
                listTagFilter[i].getAttribute('data-type') !== "publiclocal")
                listTagFilter[i].style.display = 'none';
        }
    }
});

/* =============================================== РАБОТА С РАДИОБАТОН ============================================== */

let listTag1 = document.querySelectorAll('.list-tag li');
let listFilter1 = document.querySelectorAll('.filters button');
let radiosTag = document.querySelectorAll('.tag-categories input');
let radiosFilter = document.querySelectorAll('.filter-categories input');
let selectTag = -1;
let checkedTag = 0;

let filtersArrayAll = [];
let filtersArray = [];

for (let i = 0; i < listFilter.length; i++) {
    let filtersAll = listFilter[i].innerHTML.split("<");
    filtersArrayAll.push(filtersAll[0].toLowerCase());
}

for (let i = 0; i < radiosTag.length; i++) {
    radiosTag[i].addEventListener('click', function() {
        document.getElementById('search-tag').value = '';
        let thisclick = 0;
        let check = 0;
        if (selectTag === -1)
            selectTag = i;

        if (i === selectTag) {
            if (checkedTag === 0) {
                checkedTag = 1;
                selectTag = i;
            } else {
                if (this.value)
                    thisclick = 1;
                this.checked = false;
                selectTag = -1;
                checkedTag = 0;
            }
        } else {
            selectTag = i;
            checkedTag = 1;
        }

        if (i === 1 && thisclick === 0) {
            tagsArray = [];
            for (let i = 0; i < listTag1.length; i++) {
                listTag1[i].style.display = 'inline';
                if (listTag1[i].getAttribute('data-type') !== "public")
                    listTag1[i].style.display = 'none';
                else {
                    let tagsLocal = listTag1[i].innerHTML.split("<");
                    tagsArray.push(tagsLocal[0].toLowerCase());
                }
            }
        } else if (i === 2 && thisclick === 0) {
            tagsArray = [];
            for (let i = 0; i < listTag1.length; i++) {
                listTag1[i].style.display = 'inline';
                if (listTag1[i].getAttribute('data-type') !== "local" &&
                    listTag1[i].getAttribute('data-type') !== "publiclocal")
                    listTag1[i].style.display = 'none';
                else {
                    let tagsLocal = listTag1[i].innerHTML.split("<");
                    tagsArray.push(tagsLocal[0].toLowerCase());
                }
            }
        } else if (i === 3 && thisclick === 0) {
            tagsArray = [];
            for (let i = 0; i < listTag1.length; i++) {
                listTag1[i].style.display = 'inline';
                if (listTag1[i].getAttribute('data-type') !== "publiclocal")
                    listTag1[i].style.display = 'none';
                else {
                    let tagsLocal = listTag1[i].innerHTML.split("<");
                    tagsArray.push(tagsLocal[0].toLowerCase());
                }
            }
        } else if (thisclick === 1)
            for (let i = 0; i < listTag1.length; i++)
                listTag1[i].style.display = 'inline';

        for (let i = 0; i < listTag1.length; i++)
            if (listTag1[i].getAttribute('style') === "display: none;")
                check++;

        if (check === listTag1.length)
            document.getElementById('nonresulttag').style.display = 'block';
        else
            document.getElementById('nonresulttag').style.display = 'none';
    });
}

let selectFilter = -1;
let checkedFilter = 0;

for (let i = 0; i < radiosFilter.length; i++) {
    radiosFilter[i].addEventListener('click', function() {
        document.getElementById('search-filters').value = '';
        let thisclick = 0;
        let check = 0;

        if (selectFilter === -1)
            selectFilter = i;

        if (i === selectFilter) {
            if (checkedFilter === 0) {
                checkedFilter = 1;
                selectFilter = i;
            } else {
                if (this.value)
                    thisclick = 1;
                this.checked = false;
                selectFilter = -1;
                checkedFilter = 0;
            }
        } else {
            selectFilter = i;
            checkedFilter = 1;
        }

        if (i === 0 && thisclick === 0) {
            filtersArray = [];
            for (let i = 0; i < listFilter1.length; i++) {
                listFilter1[i].style.display = '';
                if (listFilter1[i].getAttribute('data-type') !== "public")
                    listFilter1[i].style.display = 'none';
                else {
                    let tagsLocal = listFilter1[i].innerHTML.split("<");
                    filtersArray.push(tagsLocal[0].toLowerCase());
                }
            }
        } else if (i === 1 && thisclick === 0) {
            filtersArray = [];
            for (let i = 0; i < listFilter1.length; i++) {
                listFilter1[i].style.display = '';
                if (listFilter1[i].getAttribute('data-type') !== "local" &&
                    listFilter1[i].getAttribute('data-type') !== "publiclocal")
                    listFilter1[i].style.display = 'none';
                else {
                    let tagsLocal = listFilter1[i].innerHTML.split("<");
                    filtersArray.push(tagsLocal[0].toLowerCase());
                }
            }
        } else if (i === 2 && thisclick === 0) {
            filtersArray = [];
            for (let i = 0; i < listFilter1.length; i++) {
                listFilter1[i].style.display = '';
                if (listFilter1[i].getAttribute('data-type') !== "publiclocal")
                    listFilter1[i].style.display = 'none';
                else {
                    let tagsLocal = listFilter1[i].innerHTML.split("<");
                    filtersArray.push(tagsLocal[0].toLowerCase());
                }
            }
        } else if (thisclick === 1)
            for (let i = 0; i < listFilter1.length; i++)
                listFilter1[i].style.display = '';

        for (let i = 0; i < listFilter1.length; i++)
            if (listFilter1[i].getAttribute('style') === "display: none;")
                check++;

        if (check === listFilter1.length)
            document.getElementById('nonresultfilter').style.display = 'block';
        else
            document.getElementById('nonresultfilter').style.display = 'none';
    });
}

/* ========================================== РАБОТА С ТЭГАМИ И ОПЕРАТОРАМИ ========================================= */

let input = document.getElementById('input-cle').value;
let tags = document.querySelectorAll('.list-tags-for-filter button');
let operators = document.querySelectorAll('.list-operators-for-filter button');
let clear = document.querySelectorAll('.clear-cle button');

let ckeckBracket = 0;

for (let i = 0; i < tags.length; i++) {
    tags[i].addEventListener('click', function () {
        let split = input.split("  ");
        let count = split.length;
        let last = split[count - 1];
        let inputreplace = "";

        if (last === "AND" || last === "OR" || last === "NOT" || last === "(") {
            if (last === ")") {
            } else
                input += "  " + tags[i].innerHTML;
        } else {
            if (last === ")") {
            } else {
                for (let i = 1; i < split.length; i++)
                    if (inputreplace === "")
                        inputreplace += split[i - 1];
                    else
                        inputreplace += "  " + split[i - 1];

                input = inputreplace;
                if (input.length === 0)
                    input += tags[i].innerHTML;
                else
                    input += "  " + tags[i].innerHTML;
            }
        }
        document.getElementById('input-cle').value = input.toUpperCase();
    });
}

for (let i = 0; i < operators.length; i++) {
    operators[i].addEventListener('click', function () {
        let split = input.split("  ");
        let count = split.length;
        let last = split[count - 1];
        let inputreplace = "";

        if (last === "AND" || last === "OR" || last === "NOT" || last === "(" || last === ")") {
            if (operators[i].innerHTML.length === 1) {
                if (operators[i].innerHTML === "(" && last !== ")") {
                    if (operators[i].innerHTML === ")") {
                    } else
                        input += "  " + operators[i].innerHTML;

                    ckeckBracket += 1;
                }
                if (operators[i].innerHTML === ")" && last === ")")
                    if (operators[i].innerHTML === ")")
                        if (ckeckBracket > 0) {
                            input += "  " + operators[i].innerHTML;
                            ckeckBracket -= 1;
                        }
            } else {
                if (last === "(" || last === ")") {
                    if (last === ")")
                        input += "  " + operators[i].innerHTML;
                    if (last === "(") {
                        if (operators[i].innerHTML === "NOT")
                            input += "  " + operators[i].innerHTML;
                    }
                } else {
                    if (operators[i].innerHTML === "NOT") {
                        if (last === "AND" || last === "OR" || last === "(") {
                            input += "  " + operators[i].innerHTML;
                        } else { }
                    }

                    if (operators[i].innerHTML === "AND" || operators[i].innerHTML === "OR") {
                        if (last !== "NOT") {
                            for (let i = 1; i < split.length; i++)
                                if (inputreplace === "")
                                    inputreplace += split[i - 1];
                                else
                                    inputreplace += "  " + split[i - 1];

                            input = inputreplace;
                            input += "  " + operators[i].innerHTML;
                        }
                    }
                }
            }
            document.getElementById('input-cle').value = input.toUpperCase();
        } else {
            if (input.length > 0) {
                if (operators[i].innerHTML === "AND" || operators[i].innerHTML === "OR")
                    input += "  " + operators[i].innerHTML;

                if (operators[i].innerHTML === "NOT") {
                    if (last === "AND" || last === "OR" || last === "(")
                        input += "  " + operators[i].innerHTML;
                }

                if (ckeckBracket > 0)
                    if (operators[i].innerHTML === ")") {
                        input += "  " + operators[i].innerHTML;
                        ckeckBracket -= 1;
                    }
            } else {
                if (operators[i].innerHTML === "(") {
                    input += operators[i].innerHTML;
                    ckeckBracket += 1;
                }
                if (operators[i].innerHTML === "NOT") {
                    input += operators[i].innerHTML;
                }
            }
            document.getElementById('input-cle').value = input.toUpperCase();
        }
    });
}

for (let i = 0; i < clear.length; i++) {
    clear[i].addEventListener('click', function() {
        let split = input.split("  ");
        let count = split.length;
        let inputclear = "";
        let delElem;

        for (let i = 1; i < split.length; i++)
            if (inputclear === "")
                inputclear += split[i - 1];
            else
                inputclear += "  " + split[i - 1];

        delElem = split[count - 1];

        if (delElem === "(")
            ckeckBracket -= 1;
        if (delElem === ")")
            ckeckBracket += 1;

        input = inputclear;
        document.getElementById('input-cle').value = input.toUpperCase();
    });
}

let form = document.getElementById('filter-settings');
let inputNameFilter = document.getElementById('input-name-filter');
form.noValidate = true;
let preview, create = "";

window.onload = function() {
    function strPad() {
        if (this.value === "Предпросмотр") {
            preview = this.value;
            create = "";
        } else if (this.value === "Создать фильтр") {
            preview = "";
            create = this.value;
        }
    }

    let bt = document.getElementsByClassName("but");
    for (let i = 0; i < bt.length; i++) {
        bt[i].onclick = strPad;
    }
}

form.addEventListener('submit', function (e) {
    let split = input.split("  ");
    let count = split.length;
    let last = split[count - 1];

    if (preview === "Предпросмотр" || create === "Создать фильтр") {
        if (inputNameFilter.value.length === 0) {
            alert('Введите название фильтра.');
            e.preventDefault();
            inputNameFilter.focus();
        } else if (input.length === 0) {
            alert('Поле с формулой пустое!');
            e.preventDefault();
        } else if (ckeckBracket !== 0 || last === "AND" || last === "OR" || last === "NOT" || last === "(") {
            alert('Некорректная формула фильтра.');
            e.preventDefault();
        }
    }
}, false);

/* ================================================ КОНТЕКСТНОЕ МЕНЮ ================================================ */

let contextMenuTag = document.querySelectorAll(".context-menu-tag");
let contextMenuOpenTag = document.querySelector(".context-menu-tag-open");

let contextMenuFilter = document.querySelectorAll(".context-menu-filter");
let contextMenuOpenFilter = document.querySelector(".context-menu-filter-open");

for (let i = 0; i < contextMenuTag.length; i++) {
    contextMenuTag[i].addEventListener('contextmenu', function(e) {
        e.preventDefault();
        contextMenuOpenTag.style.left = e.clientX + 'px';
        contextMenuOpenTag.style.top = e.clientY + 'px';
        contextMenuOpenTag.style.display = 'block';
    });
}

for (let i = 0; i < contextMenuFilter.length; i++) {
    contextMenuFilter[i].addEventListener('contextmenu', function(e) {
        e.preventDefault();
        contextMenuOpenFilter.style.left = e.clientX + 'px';
        contextMenuOpenFilter.style.top = e.clientY + 'px';
        contextMenuOpenFilter.style.display = 'block';
    });
}

window.addEventListener('click', function() {
    contextMenuOpenTag.style.display = 'none';
    contextMenuOpenFilter.style.display = 'none';
});


let delFilterWindowOpen = document.querySelector(".deleted-filter");

document.querySelector(".del-filter").addEventListener('click', function() {
    contextMenuOpenFilter.style.display = 'none';
    delFilterWindowOpen.style.display = 'block';
    bakcgroundWindow.style.display = 'block';

    if (document.documentElement.clientWidth <= 960) {
        document.body.style.overflow = 'hidden';
        document.body.style.top = `-${window.scrollY}px`;
    }
});

window.addEventListener('scroll', function() {
    contextMenuOpenTag.style.display = 'none';
    contextMenuOpenFilter.style.display = 'none';
    if (document.documentElement.clientWidth <= 960) {
        document.body.style.overflow = 'visible';
    }
});

document.getElementById('form-tags').onscroll = function() {
    contextMenuOpenTag.style.display = 'none';
    if (document.documentElement.clientWidth <= 960) {
        document.body.style.overflow = 'visible';
    }
};
document.getElementById('form-filters').onscroll = function() {
    contextMenuOpenFilter.style.display = 'none';
    if (document.documentElement.clientWidth <= 960) {
        document.body.style.overflow = 'visible';
    }
};

/* ================================================= ОКНО АТРИБУТОВ ================================================= */

let atributWindow = document.querySelectorAll(".atributes-show");
let atributOpen = document.querySelector(".right-column-atribut");
let bakcgroundWindow = document.querySelector(".back-ground-show");

for (let i = 0; i < atributWindow.length; i++) {
    atributWindow[i].addEventListener('click', function(e) {
        e.preventDefault();
        atributOpen.style.display = 'block';
        bakcgroundWindow.style.display = 'block';

        if (document.documentElement.clientWidth <= 960) {
            document.body.style.overflow = 'hidden';
            document.body.style.top = `-${window.scrollY}px`;
        }
    });
}
document.getElementById('close-atribut').onclick = function() {
    atributOpen.style.display = 'none';
    bakcgroundWindow.style.display = 'none';
    if (document.documentElement.clientWidth <= 960) {
        document.body.style.overflow = 'visible';
    }
};

/* =============================================== ОКНО ПОДТВЕРЖДЕНИЯ =============================================== */

let delTagWindow = document.querySelectorAll(".del-tag");
let delTagWindowOpen = document.querySelector(".deleted-tag");

for (let i = 0; i < delTagWindow.length; i++) {
    delTagWindow[i].addEventListener('click', function(e) {
        e.preventDefault();
        delTagWindowOpen.style.display = 'block';
        bakcgroundWindow.style.display = 'block';

        if (document.documentElement.clientWidth <= 960) {
            document.body.style.overflow = 'hidden';
            document.body.style.top = `-${window.scrollY}px`;
        }
    });
}


document.getElementById('close-del-tag').onclick = function() {
    delTagWindowOpen.style.display = 'none';
    bakcgroundWindow.style.display = 'none';
    if (document.documentElement.clientWidth <= 960) {
        document.body.style.overflow = 'visible';
    }
};
document.getElementById('close-del-filter').onclick = function() {
    delFilterWindowOpen.style.display = 'none';
    bakcgroundWindow.style.display = 'none';
    if (document.documentElement.clientWidth <= 960) {
        document.body.style.overflow = 'visible';
    }
};

/* ================================================== ЛЕВЫЙ ХЕАДЕР ================================================== */

setInterval(function () {
    if (document.querySelector(".dropdown-li")) {
        document.getElementById('tags-link').onclick = function () {
            document.getElementById('dropdown-show').style.display = '';
        }
        document.getElementById('filter-link').onclick = function () {
            document.getElementById('dropdown-show').style.display = '';
        }

        document.getElementById('admin-link').onclick = function () {
            document.getElementById('admin-link').className = 'scrolly';
            document.getElementById('admin-us-link').className += ' active active-locked';
            document.getElementById('dropdown-show').style.display = 'block';
        }

        document.getElementById('admin-us-link').onclick = function () {
            document.getElementById('dropdown-show').style.display = 'block';
        }
        document.getElementById('admin-tf-link').onclick = function () {
            document.getElementById('dropdown-show').style.display = 'block';
        }
    }
}, 500);


document.addEventListener('DOMContentLoaded', function() {
    let table = document.querySelector('.tbl-content table');
    let headersStudent = document.querySelectorAll('.block-students .tbl-header th:not(:nth-child(1))');
    const tableBody = table.querySelector('tbody');
    const rows = tableBody.querySelectorAll('tr');

    // Направление сортировки
    const directions = Array.from(headersStudent).map(function() {
        return '';
    });

    const sortColumn = function(index) {
        const direction = directions[index] || 'asc';

        // Фактор по направлению
        const multiplier = (direction === 'asc') ? 1 : -1;

        const newRows = Array.from(rows);

        newRows.sort(function (rowA, rowB) {
            const cellA = rowA.querySelectorAll('td')[index + 1].innerHTML;
            const cellB = rowB.querySelectorAll('td')[index + 1].innerHTML;

            switch (true) {
                case cellA > cellB:
                    return 1 * multiplier;
                case cellA < cellB:
                    return -1 * multiplier;
                case cellA === cellB:
                    return 0;
            }
        });

        // Удалить старые строки
        [].forEach.call(rows, function (row) {
            tableBody.removeChild(row);
        });

        // Поменять направление
        directions[index] = direction === 'asc' ? 'desc' : 'asc';

        // Добавить новую строку
        newRows.forEach(function (newRow) {
            tableBody.appendChild(newRow);
        });

        for (let i = 0; i < headersStudent.length; i++) {
            headersStudent[i].className = "";
            if (direction === 'asc')
                headersStudent[index].className = "sort-down";
            else
                headersStudent[index].className = "sort-up";
        }
    };

    [].forEach.call(headersStudent, function(e, index) {
        e.addEventListener('click', function() {
            sortColumn(index);
        });
    });
});
