window.onresize = function() {
    if (document.documentElement.clientWidth >= 961) {
        document.body.style.overflow = 'hidden';
    } else {
        document.body.style.overflow = 'visible';
    }
};

/* ================================================= РАБОТА С ПОИСКОМ =============================================== */

document.getElementById('search-tf').onkeyup = function() {
    this.value = this.value.toLowerCase();
    let check = 0;

    document.querySelectorAll('#table-tf tr').forEach(e => {
        let s = e.textContent.toLowerCase().includes(this.value);
        if (s)
            e.style.display = '';
        else
            e.style.display = 'none';
    })

    for (let i = 0; i < document.querySelectorAll('#table-tf tr').length; i++)
        if (document.querySelectorAll('#table-tf tr')[i].getAttribute('style') === "display: none;")
            check++;

    if (check === document.querySelectorAll('#table-tf tr').length)
        document.getElementById('nonresultsttf').style.display = 'block';
    else
        document.getElementById('nonresultsttf').style.display = 'none';
};
document.getElementById('search-tf').addEventListener('search', function () {
    document.getElementById('nonresultsttf').style.display = 'none';
    for (let i = 0; i < document.querySelectorAll('#table-tf tr').length; i++)
        document.querySelectorAll('#table-tf tr')[i].style.display = '';
});

document.addEventListener('DOMContentLoaded', function() {
    let table = document.querySelector('.tbl-content table');
    let headersStudent = document.querySelectorAll('.block-tf .tbl-header th:not(:nth-last-child(1))');
    const tableBody = table.querySelector('tbody');
    const rows = tableBody.querySelectorAll('tr');

    // Направление сортировки
    const directions = Array.from(headersStudent).map(function() {
        return '';
    });

    const sortColumn = function(index) {
        const direction = directions[index] || 'asc';

        // Фактор по направлению
        const multiplier = (direction === 'asc') ? 1 : -1;

        const newRows = Array.from(rows);

        newRows.sort(function (rowA, rowB) {
            const cellA = rowA.querySelectorAll('td')[index].innerHTML;
            const cellB = rowB.querySelectorAll('td')[index].innerHTML;

            switch (true) {
                case cellA > cellB:
                    return 1 * multiplier;
                case cellA < cellB:
                    return -1 * multiplier;
                case cellA === cellB:
                    return 0;
            }
        });

        // Удалить старые строки
        [].forEach.call(rows, function (row) {
            tableBody.removeChild(row);
        });

        // Поменять направление
        directions[index] = direction === 'asc' ? 'desc' : 'asc';

        // Добавить новую строку
        newRows.forEach(function (newRow) {
            tableBody.appendChild(newRow);
        });

        for (let i = 0; i < headersStudent.length; i++) {
            headersStudent[i].className = "";
            if (direction === 'asc')
                headersStudent[index].className = "sort-down";
            else
                headersStudent[index].className = "sort-up";
        }
    };

    [].forEach.call(headersStudent, function(e, index) {
        e.addEventListener('click', function() {
            sortColumn(index);
        });
    });
});
